import "core-js/fn/string/repeat"

export type Decoder<T> = (value: any) => Result<DecodeError, T>

// Result

export type Result<Error, Value> = Ok<Value> | Err<Error>

export interface Ok<T> {
    readonly type: "Ok"
    readonly value: T
}

export interface Err<T> {
    readonly type: "Err"
    readonly error: T
}

// DecodeError

export type DecodeError = Failure | Field | Index | OneOf | Multiple

export interface Failure {
    readonly type: "Failure"
    readonly expected: string
    readonly got: any
}

export interface Field {
    readonly type: "Field"
    readonly name: string
    readonly error: DecodeError
}

export interface Index {
    readonly type: "Index"
    readonly index: number
    readonly error: DecodeError
}

export interface OneOf {
    readonly type: "OneOf"
    readonly errors: DecodeError[]
}

export interface Multiple {
    readonly type: "Multiple"
    readonly errors: DecodeError[]
}

// This function is still somewhat experimental. Let's hope it doesn't blow up
// complexity-wise
export const errorToString = (error: DecodeError) => {
    return errorHelper(0, error)
}

const errorHelper = (depth: number, error: DecodeError): string => {
    return indent(
        depth,
        ((): string => {
            switch (error.type) {
                case "Failure":
                    const stringified = JSON.stringify(error.got, null, 4)
                    const json =
                        typeof stringified !== "undefined"
                            ? stringified
                            : "undefined"
                    const gotJson = indent(1, json)
                    return `- Expected: ${error.expected}\n  Got: ${gotJson}`
                case "Field":
                    return (
                        `.${error.name}\n` + errorHelper(depth + 1, error.error)
                    )
                case "Index":
                    return (
                        `[${error.index}]\n` +
                        errorHelper(depth + 1, error.error)
                    )
                case "OneOf":
                    return (
                        "Nothing of:" +
                        error.errors
                            .map(subError => errorHelper(depth + 1, subError))
                            .join("\n")
                    )
                case "Multiple":
                    return error.errors
                        .map(subError => errorHelper(depth, subError))
                        .join("\n")
            }
        })(),
    )
}

const indent = (depth: number, text: string) =>
    text.split("\n").join("\n" + " ".repeat(4 * depth))

// Primitives

export const string = (): Decoder<string> => (value: any) =>
    typeof value === "string"
        ? { type: "Ok", value }
        : {
              type: "Err",
              error: {
                  type: "Failure",
                  expected: "string",
                  got: value,
              },
          }

export const number = (): Decoder<number> => (value: any) =>
    typeof value === "number"
        ? { type: "Ok", value }
        : {
              type: "Err",
              error: {
                  type: "Failure",
                  expected: "number",
                  got: value,
              },
          }

export const boolean = (): Decoder<boolean> => (value: any) =>
    typeof value === "boolean"
        ? { type: "Ok", value }
        : {
              type: "Err",
              error: {
                  type: "Failure",
                  expected: "boolean",
                  got: value,
              },
          }

// Data Structures

export const nullable = <T>(decoder: Decoder<T>): Decoder<T | null> =>
    oneOf([equal(null), decoder])

export const array = <T>(decoder: Decoder<T>): Decoder<T[]> => (value: any) => {
    if (!Array.isArray(value)) {
        return {
            type: "Err",
            error: {
                type: "Failure",
                expected: "array",
                got: value,
            },
        }
    } else {
        const decoded: T[] = new Array(value.length)
        const errors: DecodeError[] = []
        for (let i = 0; i < value.length; i++) {
            const result = decoder(value[i])
            switch (result.type) {
                case "Ok":
                    decoded[i] = result.value
                    break
                case "Err":
                    errors.push(result.error)
                    break
            }
        }
        if (errors.length === 0) {
            return { type: "Ok", value: decoded }
        } else {
            return {
                type: "Err",
                error: {
                    type: "Multiple",
                    errors,
                },
            }
        }
    }
}

export type DecoderObject<T> = { [P in keyof T]: Decoder<T[P]> }

export const object = <T>(decoderObject: DecoderObject<T>): Decoder<T> => (
    value: any,
) => {
    const decoded: any = {}
    const errors: DecodeError[] = []
    for (const key in decoderObject) {
        const result = decoderObject[key](value)
        switch (result.type) {
            case "Ok":
                decoded[key] = result.value
                break
            case "Err":
                errors.push(result.error)
                break
        }
    }
    if (errors.length === 0) {
        return { type: "Ok", value: decoded as T }
    } else {
        return {
            type: "Err",
            error: {
                type: "Multiple",
                errors,
            },
        }
    }
}

// export const dictionary =
//     <T>(decoder: Decoder<T>): Decoder<Map<string, T>> =>

// Object Primitives

export const field = <T>(name: string, decoder: Decoder<T>): Decoder<T> => (
    value: any,
) => {
    if (typeof value !== "object" || value === null || Array.isArray(value)) {
        return {
            type: "Err",
            error: {
                type: "Failure",
                expected: "object",
                got: value,
            },
        }
    } else {
        const result = decoder(value[name])
        return ((): Result<DecodeError, T> => {
            switch (result.type) {
                case "Ok":
                    return result
                case "Err":
                    return {
                        type: "Err",
                        error: {
                            type: "Field",
                            name,
                            error: result.error,
                        },
                    }
            }
        })()
    }
}

export const at = <T>(path: string[], decoder: Decoder<T>): Decoder<T> =>
    path
        .reverse()
        .reduce(
            (previousDecoder, name) => field(name, previousDecoder),
            decoder,
        )

// Inconsistent Structure

export const oneOf = <T>(decoders: Decoder<T>[]): Decoder<T> => (
    value: any,
) => {
    const errors: DecodeError[] = []
    for (const decoder of decoders) {
        const result = decoder(value)
        switch (result.type) {
            case "Ok":
                return result
            case "Err":
                errors.push(result.error)
                break
        }
    }
    return {
        type: "Err",
        error: {
            type: "OneOf",
            errors,
        },
    }
}

export const optional = <T>(decoder: Decoder<T>): Decoder<T | undefined> =>
    oneOf([equal(undefined), decoder])

export const maybe = <T>(decoder: Decoder<T>): Decoder<T | null> =>
    oneOf([decoder, succeed(null)])

// Mapping

export const map = <A, B>(fn: (a: A) => B, decoder: Decoder<A>): Decoder<B> => (
    value: any,
) => {
    const result = decoder(value)
    switch (result.type) {
        case "Ok":
            return {
                type: "Ok",
                value: fn(result.value),
            }
        case "Err":
            return result
    }
}

// Fancy Decoding

export const lazy = <T>(lazyDecoder: () => Decoder<T>): Decoder<T> => (
    value: any,
) => lazyDecoder()(value)

export const any = <T>(): Decoder<any> => (value: any) => ({
    type: "Ok",
    value,
})

export const forNull = <T>(replacement: T): Decoder<T> => (value: any) =>
    value === null
        ? { type: "Ok", value: replacement }
        : {
              type: "Err",
              error: {
                  type: "Failure",
                  expected: "null",
                  got: value,
              },
          }

export const forUndefined = <T>(replacement: T): Decoder<T> => (value: any) =>
    typeof value === "undefined"
        ? { type: "Ok", value: replacement }
        : {
              type: "Err",
              error: {
                  type: "Failure",
                  expected: "undefined",
                  got: value,
              },
          }

export const succeed = <T>(preset: T): Decoder<T> => (value: any) => ({
    type: "Ok",
    value: preset,
})

export const fail = (expected: string): Decoder<never> => (value: any) => ({
    type: "Err",
    error: {
        type: "Failure",
        expected,
        got: value,
    },
})

export const andThen = <A, B>(
    fn: (decoded: A) => Decoder<B>,
    decoder: Decoder<A>,
): Decoder<B> => (value: any) => {
    const result = decoder(value)
    switch (result.type) {
        case "Ok":
            return fn(result.value)(value)
        case "Err":
            return result
    }
}
export const equal = <T>(reference: T): Decoder<T> => (value: any) =>
    reference === value
        ? { type: "Ok", value }
        : {
              type: "Err",
              error: {
                  type: "Failure",
                  expected: `value equal to ${JSON.stringify(reference)}`,
                  got: value,
              },
          }
