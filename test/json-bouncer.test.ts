import {
    andThen,
    any,
    array,
    at,
    boolean,
    equal,
    fail,
    field,
    forNull,
    forUndefined,
    lazy,
    map,
    maybe,
    nullable,
    number,
    object,
    oneOf,
    optional,
    string,
    succeed,
} from "../src/json-bouncer"

describe("JSON Bouncer Test Suite", () => {
    describe("string", () => {
        it("decodes a string", () => {
            expect(string()("foobar")).toEqual({ type: "Ok", value: "foobar" })
        })
        it("fails to decoder something else", () => {
            expect(string()(7)).toEqual({
                type: "Err",
                error: {
                    type: "Failure",
                    expected: "string",
                    got: 7,
                },
            })
        })
    })
    describe("number", () => {
        it("decodes a number", () => {
            expect(number()(7)).toEqual({ type: "Ok", value: 7 })
        })
        it("fails to decode something else", () => {
            expect(number()("foobar")).toEqual({
                type: "Err",
                error: {
                    type: "Failure",
                    expected: "number",
                    got: "foobar",
                },
            })
        })
    })
    describe("boolean", () => {
        it("decodes a boolean", () => {
            expect(boolean()(true)).toEqual({ type: "Ok", value: true })
        })
        it("fails to decode something else", () => {
            expect(boolean()("foobar")).toEqual({
                type: "Err",
                error: {
                    type: "Failure",
                    expected: "boolean",
                    got: "foobar",
                },
            })
        })
    })
    describe("nullable", () => {
        it("decodes a null value", () => {
            expect(nullable(string())(null)).toEqual({
                type: "Ok",
                value: null,
            })
        })
        it("decodes a valid non-null value", () => {
            expect(nullable(string())("foobar")).toEqual({
                type: "Ok",
                value: "foobar",
            })
        })
        it("fails to decode something else", () => {
            expect(nullable(string())(7)).toEqual({
                type: "Err",
                error: {
                    type: "OneOf",
                    errors: [
                        {
                            type: "Failure",
                            expected: "value equal to null",
                            got: 7,
                        },
                        { type: "Failure", expected: "string", got: 7 },
                    ],
                },
            })
        })
    })
    describe("array", () => {
        it("decodes an empty array", () => {
            expect(array(string())([])).toEqual({ type: "Ok", value: [] })
        })
        it("decodes a valid array", () => {
            expect(array(string())(["foo", "bar", "baz"])).toEqual({
                type: "Ok",
                value: ["foo", "bar", "baz"],
            })
        })
        it("fails to decode an invalid array", () => {
            expect(array(string())(["foo", 7, null])).toEqual({
                type: "Err",
                error: {
                    type: "Multiple",
                    errors: [
                        { type: "Failure", expected: "string", got: 7 },
                        { type: "Failure", expected: "string", got: null },
                    ],
                },
            })
        })
    })
    describe("object", () => {
        it("decodes a simple object", () => {
            expect(
                object({
                    id: field("id", number()),
                    email: field("email", string()),
                    name: field("name", string()),
                })({ id: 1, email: "john.doe@example.org", name: "John Doe" }),
            ).toEqual({
                type: "Ok",
                value: {
                    id: 1,
                    email: "john.doe@example.org",
                    name: "John Doe",
                },
            })
        })
        it("fails to decode an invalid object", () => {
            expect(
                object({
                    id: field("id", number()),
                    email: field("email", string()),
                    name: field("name", string()),
                })({ id: 1, email: null }),
            ).toEqual({
                type: "Err",
                error: {
                    type: "Multiple",
                    errors: [
                        {
                            type: "Field",
                            name: "email",
                            error: {
                                type: "Failure",
                                expected: "string",
                                got: null,
                            },
                        },
                        {
                            type: "Field",
                            name: "name",
                            error: {
                                type: "Failure",
                                expected: "string",
                                got: undefined,
                            },
                        },
                    ],
                },
            })
        })
    })
    describe("field", () => {
        it("decodes an object field", () => {
            expect(field("foobar", string())({ foobar: "foobaz" })).toEqual({
                type: "Ok",
                value: "foobaz",
            })
        })
        it("fails to decode a non-object", () => {
            expect(field("foobar", string())("foobaz")).toEqual({
                type: "Err",
                error: {
                    type: "Failure",
                    expected: "object",
                    got: "foobaz",
                },
            })
        })
        it("fails to decode an object with the key missing", () => {
            expect(field("foobar", string())({ asdf: "foobaz" })).toEqual({
                type: "Err",
                error: {
                    type: "Field",
                    name: "foobar",
                    error: {
                        type: "Failure",
                        expected: "string",
                        got: undefined,
                    },
                },
            })
        })
        it("fails if the nested decoder fails", () => {
            expect(field("foobar", string())({ foobar: 7 })).toEqual({
                type: "Err",
                error: {
                    type: "Field",
                    name: "foobar",
                    error: {
                        type: "Failure",
                        expected: "string",
                        got: 7,
                    },
                },
            })
        })
    })
    describe("at", () => {
        it("decodes from a nested object", () => {
            expect(
                at(["foo", "bar"], string())({ foo: { bar: "foobar" } }),
            ).toEqual({
                type: "Ok",
                value: "foobar",
            })
        })
        it("decodes with a one-item path", () => {
            expect(at(["foobar"], string())({ foobar: "foobaz" })).toEqual({
                type: "Ok",
                value: "foobaz",
            })
        })
        it("decodes with an empty path", () => {
            expect(at([], string())("foobar")).toEqual({
                type: "Ok",
                value: "foobar",
            })
        })
        it("fails for invalid input", () => {
            expect(at(["foo", "bar"], string())({ foo: { bar: 7 } })).toEqual({
                type: "Err",
                error: {
                    type: "Field",
                    name: "foo",
                    error: {
                        type: "Field",
                        name: "bar",
                        error: {
                            type: "Failure",
                            expected: "string",
                            got: 7,
                        },
                    },
                },
            })
        })
    })
    describe("oneOf", () => {
        it("decodes a valid alternative", () => {
            expect(
                oneOf<string | number>([string(), number()])("foobar"),
            ).toEqual({ type: "Ok", value: "foobar" })
        })
        it("fails to decode something else", () => {
            expect(oneOf<string | number>([string(), number()])(null)).toEqual({
                type: "Err",
                error: {
                    type: "OneOf",
                    errors: [
                        { type: "Failure", expected: "string", got: null },
                        { type: "Failure", expected: "number", got: null },
                    ],
                },
            })
        })
    })
    describe("optional", () => {
        it("decodes an undefined value", () => {
            expect(optional(string())(undefined)).toEqual({
                type: "Ok",
                value: undefined,
            })
        })
        it("decodes a valid defined value", () => {
            expect(optional(string())("foobar")).toEqual({
                type: "Ok",
                value: "foobar",
            })
        })
        it("fails to decode something else", () => {
            expect(optional(string())(7)).toEqual({
                type: "Err",
                error: {
                    type: "OneOf",
                    errors: [
                        {
                            type: "Failure",
                            expected: "value equal to undefined",
                            got: 7,
                        },
                        { type: "Failure", expected: "string", got: 7 },
                    ],
                },
            })
        })
        it("decodes a missing object key", () => {
            expect(field("foobar", optional(string()))({})).toEqual({
                type: "Ok",
                value: undefined,
            })
        })
    })
    describe("maybe", () => {
        it("decodes an existing field", () => {
            expect(
                maybe(field("foobar", string()))({ foobar: "foobaz" }),
            ).toEqual({
                type: "Ok",
                value: "foobaz",
            })
        })
        it("decodes a missing field", () => {
            expect(maybe(field("foobar", string()))({})).toEqual({
                type: "Ok",
                value: null,
            })
        })
    })
    describe("map", () => {
        it("maps a valid value", () => {
            expect(map(value => value.length, string())("foobar")).toEqual({
                type: "Ok",
                value: 6,
            })
        })
        it("keeps the original error", () => {
            expect(map(value => value.length, string())(7)).toEqual({
                type: "Err",
                error: {
                    type: "Failure",
                    expected: "string",
                    got: 7,
                },
            })
        })
    })
    describe("lazy", () => {
        it("allows for recursive decoder definitions", () => {
            const commentDecoder = object({
                message: field("message", string()),
                replies: field("replies", array(lazy(() => commentDecoder))),
            })
            expect(
                commentDecoder({
                    message: "Hello!",
                    replies: [
                        {
                            message: "Hi there!",
                            replies: [],
                        },
                    ],
                }),
            ).toEqual({
                type: "Ok",
                value: {
                    message: "Hello!",
                    replies: [
                        {
                            message: "Hi there!",
                            replies: [],
                        },
                    ],
                },
            })
        })
    })
    describe("any", () => {
        it("decodes any value", () => {
            expect(any()("foobar")).toEqual({ type: "Ok", value: "foobar" })
        })
    })
    describe("forNull", () => {
        it("decodes a null value", () => {
            expect(forNull("foobar")(null)).toEqual({
                type: "Ok",
                value: "foobar",
            })
        })
        it("fails to decode a non-null value", () => {
            expect(forNull("foobar")(7)).toEqual({
                type: "Err",
                error: {
                    type: "Failure",
                    expected: "null",
                    got: 7,
                },
            })
        })
    })
    describe("forUndefined", () => {
        it("decodes an undefined value", () => {
            expect(forUndefined("foobar")(undefined)).toEqual({
                type: "Ok",
                value: "foobar",
            })
        })
        it("fails to decode a defined value", () => {
            expect(forUndefined("foobar")(7)).toEqual({
                type: "Err",
                error: {
                    type: "Failure",
                    expected: "undefined",
                    got: 7,
                },
            })
        })
    })
    describe("succeed", () => {
        it("always succeeds", () => {
            expect(succeed("foobar")(null)).toEqual({
                type: "Ok",
                value: "foobar",
            })
        })
    })
    describe("fail", () => {
        it("always fails", () => {
            expect(fail("something useful")(null)).toEqual({
                type: "Err",
                error: {
                    type: "Failure",
                    expected: "something useful",
                    got: null,
                },
            })
        })
    })
    describe("andThen", () => {
        it("chains decoders", () => {
            const version1Decoder = object({
                name: field("naem", string()),
            })
            const version2Decoder = object({
                name: field("name", string()),
            })
            const decoder = andThen(version => {
                switch (version) {
                    case 1:
                        return version1Decoder
                    case 2:
                        return version2Decoder
                    default:
                        return fail(`Unknown version ${version}`)
                }
            }, field("version", number()))
            expect(decoder({ version: 2, name: "John Doe" })).toEqual({
                type: "Ok",
                value: { name: "John Doe" },
            })
        })
    })
    describe("equal", () => {
        it("decodes an equal value", () => {
            expect(equal("foobar")("foobar")).toEqual({
                type: "Ok",
                value: "foobar",
            })
        })
        it("fails to decode an unequal value", () => {
            expect(equal("foobar")(7)).toEqual({
                type: "Err",
                error: {
                    type: "Failure",
                    expected: 'value equal to "foobar"',
                    got: 7,
                },
            })
        })
        it("uses strict comparison", () => {
            expect(equal(7)("7")).toEqual({
                type: "Err",
                error: {
                    type: "Failure",
                    expected: "value equal to 7",
                    got: "7",
                },
            })
        })
    })
})
